package grigory.panov;

import grigory.panov.entities.AbstractEntity;
import grigory.panov.entities.AbstractStateRecord;
import grigory.panov.entities.FEDirector;
import grigory.panov.entities.StorageGroup;
import grigory.panov.managers.FEDManager;
import grigory.panov.managers.FEDStateRecordManager;
import grigory.panov.managers.SGManager;
import grigory.panov.utils.ApachePoiUtils;
import grigory.panov.utils.ProblemAnalyzer;
import grigory.panov.utils.Utils;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@Controller
public class MainController {

    private static final Double DEFAULT_QUEUE_THRESHOLD = 6.0;
    private static final Integer INTERNAL_SERVER_ERROR = 500;
    private static final String FED_REQUEST_TYPE_MARK = "F";
    private static final String SG_REQUEST_TYPE_MARK = "S";

    @RequestMapping("/")
    public String run() {
        return "index";
    }

    @RequestMapping("/compare")
    public String compare() {
        return "compare";
    }

    @RequestMapping("/getChartDots")
    public void getChartDots(HttpServletResponse response, @RequestParam(value = "entityKey", defaultValue = "f1") String entityKey) throws IOException {
        String type = String.valueOf(entityKey.charAt(0));
        List<Double> responseArray = new ArrayList<>();
        if (FED_REQUEST_TYPE_MARK.equals(type)) {
            entityKey = entityKey.replaceFirst(FED_REQUEST_TYPE_MARK, "");
            if (Utils.tryParseInt(entityKey)){
                responseArray = prepareDotsListForChart(FEDManager.getInstance().getEntityByKey(Integer.parseInt(entityKey)));
            }
        }else if (SG_REQUEST_TYPE_MARK.equals(type)) {
            entityKey = entityKey.replaceFirst(SG_REQUEST_TYPE_MARK, "");
            if (Utils.tryParseInt(entityKey)){
                responseArray = prepareDotsListForChart(SGManager.getInstance().getEntityByKey(Integer.parseInt(entityKey)));
            }
        }else {
            throw new IllegalArgumentException("Request doesn't contain correct entity mark");
        }
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        JSONArray mJSONArray = new JSONArray(responseArray);
        out.println(mJSONArray);
        out.flush();
    }

    @RequestMapping("/getOptions")
    public void getRequest(HttpServletResponse response) throws IOException {
        FEDManager feDManager = FEDManager.getInstance();
        SGManager sgManager = SGManager.getInstance();
        List<String> arrayList = feDManager.getEntitiesAsString();
        arrayList.addAll(sgManager.getEntitiesAsString());
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        JSONArray mJSONArray = new JSONArray(arrayList);
        out.println(mJSONArray);
        out.flush();
    }

    @RequestMapping("/analyze")
    public String processRequest(Model model, @RequestParam(value = "threshold", defaultValue = "6.0") String threshold) {
        FEDManager feDManager = FEDManager.getInstance();
        SGManager sgManager = SGManager.getInstance();
/*        FEDStateRecordManager feDStateRecordManager = FEDStateRecordManager.getInstance();
        SGStateRecordManager sgStateRecordManager = SGStateRecordManager.getInstance();*/
        Double actualThreshold = DEFAULT_QUEUE_THRESHOLD;
        if (Utils.tryParseDouble(threshold)) {
            actualThreshold = Double.parseDouble(threshold);
        }
        ProblemAnalyzer problemAnalyzer = new ProblemAnalyzer(actualThreshold);

        HashMap<FEDirector, StorageGroup> analyzeResult = problemAnalyzer.analyzeFEDs(FEDManager.getInstance().getEntities(), SGManager.getInstance().getEntities());

        model.addAttribute("SGs", sgManager.getEntitiesAsHtml());
        model.addAttribute("FEDs", feDManager.getEntitiesAsHtml());
        Set<FEDirector> feDs = analyzeResult.keySet();

        String resolution;
        if (feDs.size() == 0) {
            resolution = "No one FE Director has problems";
        } else if (feDs.size() == 1) {
            resolution = String.format("FE Director %s has problems",feDs.iterator().next().getId());
        } else {
            StringBuilder sb = new StringBuilder();
            for (FEDirector feD : feDs) {
                sb.append(feD.getId()).append(", ");
            }
            sb.delete(sb.length() - 2, sb.length());
            resolution = String.format("FE Directors %s have problems", sb.toString());
        }
        model.addAttribute("problemFEDs", resolution);

        if (feDs.size() == 0) {
            resolution = "No one FE Director has problems";
        } else {
            StringBuilder sb = new StringBuilder();
            for (FEDirector feD : feDs) {
                sb.append(String.format("Storage Group %s spoils work of FE Directors %s <br>",
                        analyzeResult.get(feD).getId(), feD.getId() ));
            }
            resolution = sb.toString();
        }
        model.addAttribute("infSGs", resolution);
        return "analyze";
    }

    @RequestMapping("/downloadXLSX")
    public void downloadXLSX(HttpServletResponse response) throws IOException {
        Workbook workbook;
        try {
            workbook = ApachePoiUtils.prepareXLSX(FEDManager.getInstance().getEntities(), SGManager.getInstance().getEntities());
        } catch (IOException e) {
            response.sendError(INTERNAL_SERVER_ERROR, "Something went wrong during Charts.xlsx preparation");
            return;
        }
        String fileName = "Charts.xlsx";
        ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
        workbook.write(outByteStream);
        byte[] outArray = outByteStream.toByteArray();
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setContentLength(outArray.length);
        response.setHeader("Expires:", "0"); // eliminates browser caching
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
        OutputStream outStream = response.getOutputStream();
        outStream.write(outArray);
        outStream.flush();
    }

    /**
     * abstractEntity
     * @return ArrayList of dots in the format: 1, firstDot, 2, secondDot. This format is required for google charts
     * In future can be moved to AbstractEntitySRProvider, currently not enough functionality for new class
     */
    private ArrayList<Double> prepareDotsListForChart(AbstractEntity abstractEntity) {
        Integer expectedArrSize = FEDStateRecordManager.getInstance().getMaxRecordCount();
        List abstractEntitySRs = abstractEntity.getStateRecords();
        if (CollectionUtils.isEmpty(abstractEntitySRs)){
            return new ArrayList<>();
        }
        ArrayList<Double> resultArray = new ArrayList<>();
        for (int i = 0; i < abstractEntitySRs.size(); i++){
            AbstractStateRecord abstractEntitySR = (AbstractStateRecord) abstractEntitySRs.get(i);
            resultArray.add(i + 1.0);
            resultArray.add(abstractEntitySR.getAverageValue());
        }
        for (int i = abstractEntitySRs.size(); i < expectedArrSize; i++){
            Double avgBasketNumber = -1.0;
            resultArray.add(i + 1.0);
            resultArray.add(avgBasketNumber);
        }
        return resultArray;
    }
}
