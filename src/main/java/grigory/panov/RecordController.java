package grigory.panov;

import grigory.panov.entities.FEDStateRecord;
import grigory.panov.entities.FEDirector;
import grigory.panov.entities.SGStateRecord;
import grigory.panov.entities.StorageGroup;
import grigory.panov.managers.FEDManager;
import grigory.panov.managers.FEDStateRecordManager;
import grigory.panov.managers.SGManager;
import grigory.panov.managers.SGStateRecordManager;
import grigory.panov.utils.Utils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class RecordController {

    @RequestMapping("/addFRD")
    public String addFRD(@RequestParam(value = "key") String key,
                      @RequestParam(value = "id") String id){
        if (key == null || id == null || !Utils.tryParseInt(key) || Integer.parseInt(key) <= 0) {
            throw new IllegalArgumentException("Incorrect FE Director key");
        }
        FEDManager feDManager = FEDManager.getInstance();
        FEDirector feDirector = new FEDirector();
        feDirector.setKey(Integer.parseInt(key));
        feDirector.setId(id);
        feDManager.addEntity(feDirector);
        return "index";
    }

    @RequestMapping("/addSG")
    public String addSG(@RequestParam(value = "key") String key,
                      @RequestParam(value = "id") String id){
        SGManager sgManager = SGManager.getInstance();
        if (key == null || id == null || !Utils.tryParseInt(key)) {
            throw new IllegalArgumentException("Incorrect Storage Group key");
        }
        StorageGroup storageGroup = new StorageGroup();
        storageGroup.setKey(Integer.parseInt(key));
        storageGroup.setId(id);
        sgManager.addEntity(storageGroup);
        return "index";
    }

    //TODO: add full list of params (maybe as mapped object);
    @RequestMapping("/addFEDRecord")
    public String addFEDRecord(@RequestParam(value = "key") String key,
                               @RequestParam(value = "avgBucket") String avgBucket){
        if (key == null || avgBucket == null || !Utils.tryParseInt(key) ||
                !Utils.tryParseDouble(avgBucket)) {
            throw new IllegalArgumentException("Incorrect FE Directory key");
        }
        final Long fakeTimeKey = 239L;
        FEDStateRecord feDStateRecord = new FEDStateRecord(fakeTimeKey, Integer.parseInt(key), Double.parseDouble(avgBucket));
        FEDStateRecordManager.getInstance().addRecord(feDStateRecord);
        return "index";
    }

    //TODO: add full list of params (maybe as mapped object);
    @RequestMapping("/addSGRecord")
    public String addSGRecord(@RequestParam(value = "key") String key,
                               @RequestParam(value = "avgBucket") String avgBucket){
        if (key == null || avgBucket == null || !Utils.tryParseInt(key) ||
                !Utils.tryParseDouble(avgBucket)) {
            throw new IllegalArgumentException("Incorrect Storage Group key");
        }
        final Long fakeTimeKey = 239L;
        SGStateRecord sgStateRecord = new SGStateRecord(fakeTimeKey, Integer.parseInt(key), Double.parseDouble(avgBucket));
        SGStateRecordManager.getInstance().addRecord(sgStateRecord);
        return "index";
    }
}

