package grigory.panov.entities;

import java.util.List;

public abstract class AbstractEntity<T extends AbstractStateRecord> {

    private Integer key;
    private String id;

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public abstract List<T> getStateRecords();

    @Override
    public String toString() {
        return String.format("[%s] id: \"%s\"; key: \"%d\"", this.getClass().toString(), this.getId(), this.getKey());
    }

}
