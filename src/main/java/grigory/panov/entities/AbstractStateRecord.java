package grigory.panov.entities;

public abstract class AbstractStateRecord {

    private Long timeKey;
    private Integer key;

    AbstractStateRecord(Long timeKey, Integer key) {
        this.timeKey = timeKey;
        this.key = key;
    }

    public Long getTimeKey() {
        return timeKey;
    }

    public Integer getKey() {
        return key;
    }

    public abstract Double getAverageValue();

    @Override
    public String toString(){
        return String.format("[abstractStateRecord] timeKey: %d; key: %d; avgBucket: %f", this.getTimeKey(),
                this.getKey(), this.getAverageValue());
    }

}
