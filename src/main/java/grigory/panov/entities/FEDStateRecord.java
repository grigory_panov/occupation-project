package grigory.panov.entities;

public class FEDStateRecord extends AbstractStateRecord{

    private Double averageQueueBucket;

    public FEDStateRecord(Long timeKey, Integer feDirectorKey, Double averageQueueBucket) {
        super(timeKey, feDirectorKey);
        this.averageQueueBucket = averageQueueBucket;
    }

    @Override
    public String toString(){
        return String.format("[FEDStateRecord] timeKey: %d; feDirectorKey: %d; sumQueueDepCount: %f", this.getTimeKey(),
                this.getKey(), this.getAverageValue());
    }

    @Override
    public Double getAverageValue() {
        return averageQueueBucket;
    }
}