package grigory.panov.entities;

import grigory.panov.utils.JDBCUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.persistence.Transient;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class FEDirector extends AbstractEntity<FEDStateRecord> implements Serializable {
    public static final String TYPE = "FE Director";
    private static Logger log = Logger.getLogger(FEDirector.class.getName());
    //TODO: Move query to external file
    private final String GET_FED_STATE_RECORDS_QUERY = ""
            + "select 	fed.timekey as time_key, fed.fedirectorkey as fed_key, "
            + "	fed.spmqueuedepcount0 as q0, fed.spmqueuedepcount1 as q1, "
            + "	fed.spmqueuedepcount2 as q2, fed.spmqueuedepcount3 as q3, "
            + "	fed.spmqueuedepcount4 as q4, fed.spmqueuedepcount5 as q5, "
            + "	fed.spmqueuedepcount6 as q6, fed.spmqueuedepcount7 as q7, "
            + "	fed.spmqueuedepcount8 as q8, fed.spmqueuedepcount9 as q9 "
            + "from dwf_fedirector_r fed "
            + "where fed.fedirectorkey = %d";

    public FEDirector() {}

    @Override
    @Transient
    public List<FEDStateRecord> getStateRecords() {
        List<FEDStateRecord> entitySRs = new ArrayList<>();
        try {
            Connection dbConnection = JDBCUtils.getConnection();
            Statement st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(String.format(GET_FED_STATE_RECORDS_QUERY, this.getKey()));
            while (rs.next()) {
                Integer feDKey = rs.getInt("fed_key");
                Long timeKey = rs.getLong("time_key");
                Double requestCount = 0.0;
                Double requestsPerBaskets = 0.0;
                for (Integer basNum = 0; basNum <= 9; basNum++) {
                    Double requestsPerBasket = rs.getDouble("q" + basNum.toString());
                    requestCount += requestsPerBasket;
                    requestsPerBaskets += requestsPerBasket * (basNum + 1);
                }
                Double averageQueueBasket = 0.0;
                if (requestCount != 0) {
                    averageQueueBasket = requestsPerBaskets / requestCount;
                }
                entitySRs.add(new FEDStateRecord(timeKey, feDKey, averageQueueBasket));
            }
            JDBCUtils.putConnection(dbConnection);
        } catch (SQLException e) {
            log.log(Level.ERROR, "Couldn't get FE Director State Records From DataBase: ", e);
            throw new RuntimeException("Couldn't get FE Director State Records From DataBase: " + e.getMessage());
        }
        log.info(String.format("State Records for FE Director# %d was successfully taken", this.getKey()));
        return entitySRs;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FEDirector other = (FEDirector) obj;
        return this.getKey().equals(other.getKey());
    }

    @Override
    public int hashCode() {
        final int prime = 239;
        int result = 2;
        result = prime * result + getKey();
        return result;
    }
}