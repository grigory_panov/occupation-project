package grigory.panov.entities;

public class SGStateRecord extends AbstractStateRecord{

    private Double avgWaitTimePerReqNumber;

    public SGStateRecord(Long timeKey, Integer sgKey, Double avgWaitTimePerReqNumber) {
        super(timeKey, sgKey);
        this.avgWaitTimePerReqNumber = avgWaitTimePerReqNumber;
    }

    @Override
    public String toString(){
        return String.format("[SGStateRecord] timeKey: %d; feDirectorKey: %d; sumWaitTime: %f", this.getTimeKey(),
                this.getKey(), this.getAverageValue());
    }

    @Override
    public Double getAverageValue() {
        return avgWaitTimePerReqNumber;
    }
}