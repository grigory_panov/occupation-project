package grigory.panov.entities;

import grigory.panov.utils.JDBCUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Entity
public class StorageGroup extends AbstractEntity<SGStateRecord> implements Serializable {
    public static final String TYPE = "StorageGroup";
    private static Logger log = Logger.getLogger(StorageGroup.class.getName());

    //TODO: move query to external file
    private final String GET_SG_STATE_RECORD_QUERY = ""
            + " select 	sg.timekey as time_key, sg.storagegroupkey as sg_key, "
            + "		sg.spmreadrtcount1 as r1, sg.spmreadrtcount2 as r2, "
            + "		sg.spmreadrtcount3 as r3, sg.spmreadrtcount4 as r4, "
            + "		sg.spmreadrtcount5 as r5, sg.spmreadrtcount6 as r6, "
            + "		sg.spmreadrtcount7 as r7, sg.spmwritertcount1 as w1, "
            + "		sg.spmwritertcount2 as w2, sg.spmwritertcount3 as w3, "
            + "		sg.spmwritertcount4 as w4, sg.spmwritertcount5 as w5, "
            + "		sg.spmwritertcount6 as w6, sg.spmwritertcount7 as w7 "
            + " from dwf_storagegroup_r sg" +
            " where sg.storagegroupkey = %d";

    public StorageGroup() {}

    @Override
    @Transient
    public List<SGStateRecord> getStateRecords() {
        List<SGStateRecord> entitySRs = new ArrayList<>();
        try {
            Connection dbConnection = JDBCUtils.getConnection();
            Statement st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(String.format(GET_SG_STATE_RECORD_QUERY, this.getKey()));
            while (rs.next()) {
                Double waitBaskPerRequests = 0.0;
                for (Integer basNum = 1; basNum <= 7; basNum++) {
               /* Only for case if there is the same queue in FE Director for both write and read request*/
                    waitBaskPerRequests += rs.getDouble("r" + basNum.toString()) * basNum;
                    waitBaskPerRequests += rs.getDouble("w" + basNum.toString()) * basNum;
                }
                Integer sgKey = rs.getInt("sg_key");
                Long timeKey = rs.getLong("time_key");
                entitySRs.add(new SGStateRecord(timeKey, sgKey, waitBaskPerRequests));
            }
            JDBCUtils.putConnection(dbConnection);
        } catch (SQLException e) {
            log.log(Level.ERROR, "Couldn't get Storage Group State Records From DataBase: ", e);
            throw new RuntimeException("Couldn't get Storage Group State Records From DataBase: " + e.getMessage());
        }
        log.info("EntityStateRecordsList was successfully returned ");
        return entitySRs;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        StorageGroup other = (StorageGroup) obj;
        return this.getKey().equals(other.getKey());
    }

    @Override
    public int hashCode() {
        final int prime = 932;
        int result = 2;
        result = prime * result + getKey();
        return result;
    }
}