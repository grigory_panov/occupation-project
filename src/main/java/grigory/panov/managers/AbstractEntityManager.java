package grigory.panov.managers;

import grigory.panov.entities.AbstractEntity;
import grigory.panov.utils.HibernateUtil;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractEntityManager<T extends AbstractEntity> {
    private static Logger log = Logger.getLogger(AbstractEntityManager.class.getName());

    abstract String getManagedEntityType();

    public Integer entityCount(){
        return getEntities().size();
    }

    public String getEntitiesAsHtml(){
        StringBuilder sb = new StringBuilder();
        List<T> entitiesList = getEntities();
        for(T entity : entitiesList) {
            sb.append(String.format("[%s] id: \"%s\"; key: \"%d\"<br>", getManagedEntityType(), entity.getId(), entity.getKey()));
        }
        return sb.toString();
    }

    public List<String> getEntitiesAsString() {
        ArrayList<String> resultList = new ArrayList<>();
        List<T> entitiesList = getEntities();
        for(T entity : entitiesList) {
            resultList.add(String.format("[%s] id: \"%s\"; key: %d", getManagedEntityType(), entity.getId(), entity.getKey()));
        }
        return resultList;
    }

    public void addEntity(T entity) {
        Integer expectedEntityKey = entityCount() + 1;
        if (entity.getKey().equals(expectedEntityKey)) {
            Session session = HibernateUtil.getSession();
            Transaction tx = session.beginTransaction();
            session.save(entity);
            session.flush();
            session.clear();
            tx.commit();
            HibernateUtil.putSession(session);
            log.info(String.format("Entity with key: %d and id: \"%s\" was successfully added", entity.getKey(), entity.getId()));

        }else {
            String errMsg = String.format("Incorrect entity key! Expected entityKey = %s; Actual entityKey = %d", expectedEntityKey, entity.getKey());
            log.log(Level.ERROR, errMsg);
            throw new IllegalArgumentException(errMsg);
        }
    }

    public abstract List<T> getEntities();

    public T getEntityByKey(Integer key){
        List<T> entities = getEntities();
        for (T entity : entities){
            if (entity.getKey().equals(key)){
                return entity;
            }
        }
        return null;
    }
}
