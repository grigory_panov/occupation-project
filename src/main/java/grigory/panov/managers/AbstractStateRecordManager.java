package grigory.panov.managers;

import grigory.panov.entities.AbstractStateRecord;
import grigory.panov.utils.JDBCUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class AbstractStateRecordManager<T extends AbstractStateRecord> {
    private static Logger log = Logger.getLogger(AbstractStateRecordManager.class.getName());

    //TODO: move query to external file
    private static final String GET_MAX_RECORDS_COUNT_QUERY = ""
            + "WITH count_t AS( "
            + "    SELECT count(1) AS count "
            + "    FROM dwf_storagegroup_r "
            + "    GROUP BY storagegroupkey "
            + "    union "
            + "    SELECT count(1) AS count "
            + "    FROM dwf_fedirector_r "
            + "    GROUP BY fedirectorkey) "
            + "SELECT max(count_t.count) as count FROM count_t";

    public Integer getMaxRecordCount(){
        Integer maxCount = 0;
        try {
            Connection dbConnection = JDBCUtils.getConnection();
            Statement st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(GET_MAX_RECORDS_COUNT_QUERY);
            while (rs.next()) {
                maxCount = rs.getInt("count");
            }
            JDBCUtils.putConnection(dbConnection);
        } catch (SQLException e) {
            log.log(Level.ERROR, "Couldn't get MaxEntitiesRecordsCount From DataBase: ", e);
            throw new RuntimeException("Couldn't get MaxEntitiesRecordsCount From DataBase: " + e.getMessage());
        }
        log.info(String.format("Current MaxEntitiesRecordsCount = %d ", maxCount));
        return maxCount;
    }

    public abstract void addRecord(T abstractStateRecord);

}
