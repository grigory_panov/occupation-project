package grigory.panov.managers;

import grigory.panov.entities.FEDirector;
import grigory.panov.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FEDManager extends AbstractEntityManager<FEDirector>{

    private static FEDManager instance;

    private FEDManager(){}

    public static synchronized FEDManager getInstance(){
        if (instance == null){
            instance = new FEDManager();
        }
        return instance;
    }
    @Override
    public List<FEDirector> getEntities() {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        Query<FEDirector> qry = session.createQuery("from FEDirector", FEDirector.class);
        List<FEDirector>feDirectorsList = qry.list();
        session.getTransaction().commit();
        HibernateUtil.putSession(session);
        return feDirectorsList;
    }

    @Override
    public String getManagedEntityType() {
        return FEDirector.TYPE;
    }


}
