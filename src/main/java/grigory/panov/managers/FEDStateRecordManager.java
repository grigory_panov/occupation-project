package grigory.panov.managers;

import grigory.panov.entities.FEDStateRecord;
import grigory.panov.entities.FEDirector;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.List;

public class FEDStateRecordManager extends AbstractStateRecordManager<FEDStateRecord>{

    private static Logger log = Logger.getLogger(FEDStateRecordManager.class.getName());
    private static FEDStateRecordManager instance;

    private FEDStateRecordManager(){ }

    public static synchronized FEDStateRecordManager getInstance(){
        if (instance == null){
            instance = new FEDStateRecordManager();
        }
        return instance;
    }

    @Override
    public void addRecord(FEDStateRecord fedStateRecord) {
        {
            List<FEDirector> entitySRList = FEDManager.getInstance().getEntities();
            if (0 < fedStateRecord.getKey() || fedStateRecord.getKey() <= entitySRList.size()){
                //TODO:Implement insert to database
            }else{
                String errMsg = String.format("Incorrect entity key! Expected entityKey = %s, Actual entityKey = %d",
                        entitySRList.size(), fedStateRecord.getKey());
                log.log(Level.ERROR, errMsg);
                throw new IllegalArgumentException(errMsg);
            }
        }
    }

}
