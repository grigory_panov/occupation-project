package grigory.panov.managers;

import grigory.panov.entities.StorageGroup;
import grigory.panov.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class SGManager extends AbstractEntityManager<StorageGroup>{

    private static SGManager instance;

    private SGManager() {}

    public static synchronized SGManager getInstance(){
        if (instance == null){
            instance = new SGManager();
        }
        return instance;
    }

    @Override
    public String getManagedEntityType() {
        return StorageGroup.TYPE;
    }

    @Override
    public List<StorageGroup> getEntities() {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        Query<StorageGroup> qry = session.createQuery("from StorageGroup", StorageGroup.class);
        List<StorageGroup> sgList = qry.list();
        session.getTransaction().commit();
        HibernateUtil.putSession(session);
        return sgList;
    }
}
