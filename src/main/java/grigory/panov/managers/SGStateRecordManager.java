package grigory.panov.managers;

import grigory.panov.entities.SGStateRecord;
import grigory.panov.entities.StorageGroup;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.List;

public class SGStateRecordManager extends AbstractStateRecordManager<SGStateRecord>{

    private static Logger log = Logger.getLogger(SGStateRecordManager.class.getName());
    private SGStateRecordManager(){}

    private static SGStateRecordManager instance;

    public static synchronized SGStateRecordManager getInstance(){
        if (instance == null){
            instance = new SGStateRecordManager();
        }
        return instance;
    }

    @Override
    public void addRecord(SGStateRecord abstractStateRecord) {
        {
            List<StorageGroup> sgList = SGManager.getInstance().getEntities();
            if (0 < abstractStateRecord.getKey() || abstractStateRecord.getKey() <= sgList.size()) {
                //TODO:Implement insert to database
            } else {
                String errMsg = String.format("Incorrect entity key! Expected entityKey = %s, Actual entityKey = %d",
                        sgList.size(), abstractStateRecord.getKey());
                log.log(Level.ERROR, errMsg);
                throw new IllegalArgumentException(errMsg);
            }
        }
    }

}
