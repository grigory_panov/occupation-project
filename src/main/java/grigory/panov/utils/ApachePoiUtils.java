package grigory.panov.utils;

import grigory.panov.entities.FEDStateRecord;
import grigory.panov.entities.FEDirector;
import grigory.panov.entities.SGStateRecord;
import grigory.panov.entities.StorageGroup;
import grigory.panov.managers.AbstractStateRecordManager;
import grigory.panov.managers.FEDStateRecordManager;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.charts.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.util.List;

public class ApachePoiUtils {
    private static final String CELL_NUMERIC_FORMAT = "0.00";
    public static Workbook prepareXLSX(List<FEDirector> feDEntities, List<StorageGroup> sgEntities) throws IOException {
        Workbook wb = new XSSFWorkbook();
        XSSFSheet sheet = (XSSFSheet) wb.createSheet();
        XSSFRow row = sheet.createRow((short) 0);
        CellStyle style = wb.createCellStyle();
        Integer maxRecordCount = FEDStateRecordManager.getInstance().getMaxRecordCount();

        style.setDataFormat(HSSFDataFormat.getBuiltinFormat(CELL_NUMERIC_FORMAT));

        /* Create Cell Number part */
        row.createCell(0).setCellValue("#");
        for (int i = 0; i < maxRecordCount; i++) {
            Cell c = row.createCell(i + 1);
            c.setCellType(CellType.NUMERIC);
            c.setCellValue(i + 1);
        }

        /* Create FE Directors Total Queue part */
        for (int listId = 0; listId < feDEntities.size(); listId++) {
            row = sheet.createRow((short) listId + 1);
            row.createCell(0).setCellValue(String.format("FE Director# %d", listId + 1));
            List<FEDStateRecord> feDStateRecords = feDEntities.get(listId).getStateRecords();
            for (int recordId = 0; recordId < feDStateRecords.size(); recordId++){
                Cell c = row.createCell(recordId + 1);
                c.setCellStyle(style);
                c.setCellValue(feDStateRecords.get(recordId).getAverageValue());
            }
        }

        /* Create Storage Groups Total Wait Time part */
        for (int listId = 0; listId < sgEntities.size(); listId++) {
            row = sheet.createRow((short) listId + feDEntities.size() + 1);
            row.createCell(0).setCellValue(String.format("Storage Group# %d", listId + 1));
            List<SGStateRecord> sgStateRecords = sgEntities.get(listId).getStateRecords();
            for (int recordId = 0; recordId < sgStateRecords.size(); recordId++){
                Cell c = row.createCell(recordId + 1);
                c.setCellStyle(style);
                c.setCellValue(sgStateRecords.get(recordId).getAverageValue());
            }
            for (int recordId = sgStateRecords.size(); recordId < maxRecordCount; recordId++){
                Cell c = row.createCell(recordId + 1);
                c.setCellStyle(style);
                c.setCellValue(-1.0);
            }
        }

        for (int i = 0; i < feDEntities.size() + sgEntities.size(); i++){
            Drawing drawing = sheet.createDrawingPatriarch();
            Integer dx1 = 0;
            Integer dy1 = 0;
            Integer dx2 = 0;
            Integer dy2 = 0;
            Integer col1 = 1;
            Integer col2 = 15;
            Integer row1 = 35 + (i * 25);
            Integer row2 = 55 + (i * 25);
            ClientAnchor anchor = drawing.createAnchor(dx1, dy1,dx2, dy2, col1, row1, col2, row2);

            Chart chart = drawing.createChart(anchor);
            ChartLegend legend = chart.getOrCreateLegend();
            legend.setPosition(LegendPosition.RIGHT);
            LineChartData data = chart.getChartDataFactory().createLineChartData();
            ChartAxis bottomAxis = chart.getChartAxisFactory().createCategoryAxis(AxisPosition.BOTTOM);
            ValueAxis leftAxis = chart.getChartAxisFactory().createValueAxis(AxisPosition.LEFT);
            leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);
            Integer firstRow = 0, lastRow = 0;
            Integer firstCol = 1;
            ChartDataSource<Number> xs = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(firstRow, lastRow, firstCol, maxRecordCount));
            firstRow = lastRow = i + 1;
            ChartDataSource<Number> ys1 = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(firstRow, lastRow, firstCol, maxRecordCount));
            LineChartSeries series = data.addSeries(xs, ys1);
            series.setTitle(sheet.getRow(firstRow).getCell(0).getStringCellValue());
            chart.plot(data, bottomAxis, leftAxis);
        }
        return wb;
    }
}
