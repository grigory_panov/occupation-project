package grigory.panov.utils;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

public class JDBCUtils {
    private static final BasicDataSource ds = new BasicDataSource();
    private static Logger log = Logger.getLogger(JDBCUtils.class.getName());
    /**
     * SetEnv:
     * export Q1_DB_CONNECTION="jdbc:postgresql://localhost:5432/q1"
     * export Q1_DB_USERNAME="username"
     * export Q1_DB_PASSWORD="password"
     */
    static {
        Map<String, String> env = System.getenv();
        String password = env.get("Q1_DB_PASSWORD");
        String userName = env.get("Q1_DB_USERNAME");
        String sqlURL = env.get("Q1_DB_CONNECTION");
        ds.setPassword(password);
        ds.setUrl(sqlURL);
        ds.setUsername(userName);
    }

    public static Connection getConnection(){
        try {
            Connection dbConnection = ds.getConnection();
            log.info("dbConnection was successfully taken");
            return dbConnection;
        } catch (SQLException e) {
             log.log(Level.ERROR, "dbConnection wasn't taken. Exception: ", e);
            throw new RuntimeException("Can't get new connection");
        }
    }
    public static void putConnection(Connection connection) throws SQLException {
        if (connection !=null && !connection.isClosed()) {
            connection.close();
        }
    }



}

