package grigory.panov.utils;

import grigory.panov.entities.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.*;

public class ProblemAnalyzer {
    private static Logger log = Logger.getLogger(ProblemAnalyzer.class.getName());
    private static String SG_DOESNT_CONTAIN_ERR_MSG = "For FE Director with key: %d, collection of Storage Groups doesn't contain info about time %s";
    private final Double queueThreshold;

    public ProblemAnalyzer(Double queueThreshold){
        this.queueThreshold = queueThreshold;
    }

    public HashMap<FEDirector, StorageGroup> analyzeFEDs(List<FEDirector> feDEntities, List<StorageGroup> sgEntities){
        HashMap<FEDirector, StorageGroup> infSGs = new HashMap<>();
        for (FEDirector feDirector : feDEntities) {
            List<FEDStateRecord> feDStateRecords = feDirector.getStateRecords();
            StorageGroup infSG = findInfSGForFED(feDStateRecords, sgEntities);
            if (infSG != null) {
                infSGs.put(feDirector, infSG);
            }
        }
        log.info("analyzeFEDs return: " + infSGs);
        return infSGs;
    }

    private StorageGroup findInfSGForFED(List<FEDStateRecord> feDStateRecords, List<StorageGroup> sgEntities){
        Integer[] localRate = new Integer[sgEntities.size()];
        Arrays.fill(localRate, 0);
        Boolean hasProblem = false;
        Map<StorageGroup, List<SGStateRecord>> sgRecords = new HashMap<>();
        for (StorageGroup sgEntity : sgEntities){
            sgRecords.put(sgEntity, sgEntity.getStateRecords());
        }
        for (AbstractStateRecord feDStateRecord : feDStateRecords){
            if (feDStateRecord.getAverageValue() > queueThreshold) {
                hasProblem = true;
                StorageGroup affectedSG = findAffectedSGByTime(sgRecords, feDStateRecord.getTimeKey());
                if (affectedSG == null) {
                    String errMsg = String.format(SG_DOESNT_CONTAIN_ERR_MSG, feDStateRecord.getKey(), feDStateRecord.getTimeKey());
                    log.log(Level.ERROR , errMsg);
                    throw new RuntimeException(errMsg);
                }
                localRate[affectedSG.getKey() - 1]++;
            }
        }

        Integer infSGKey = -1;
        Integer infSGRate = -1;
        if (hasProblem) {
            for (int sgKey = 0; sgKey < localRate.length; sgKey++) {
                if (localRate[sgKey] > infSGRate) {
                    infSGRate = localRate[sgKey];
                    infSGKey = sgKey + 1;
                }
            }
        }
        log.info(String.format("Storage Group %d spoils work of FE Director %d",infSGKey, feDStateRecords.get(0).getKey()));
        if (infSGKey > 0) {
            return sgEntities.get(infSGKey - 1);
        }else {
            return null;
        }
    }

    private StorageGroup findAffectedSGByTime(Map<StorageGroup, List<SGStateRecord>> sgRecords, Long timeKey){
        Double maxSumTime = 0.0;
        StorageGroup affectedSG = null;
        Set<StorageGroup> sgEntities = sgRecords.keySet();
        for (StorageGroup sgEntity : sgEntities) {
            List<SGStateRecord> sgStateRecords = sgRecords.get(sgEntity);
            for (SGStateRecord sgStateRecord : sgStateRecords){
                if (sgStateRecord.getTimeKey().equals(timeKey) && maxSumTime < sgStateRecord.getAverageValue()){
                    maxSumTime = sgStateRecord.getAverageValue();
                    affectedSG = sgEntity;
                    break;
                }else if (sgStateRecord.getTimeKey().equals(timeKey) && maxSumTime > sgStateRecord.getAverageValue()){
                    break;
                }
            }
        }
        return affectedSG;
    }

}
