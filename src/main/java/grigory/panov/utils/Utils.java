package grigory.panov.utils;

public class Utils {
    public static boolean tryParseDouble(String input){
        try {
            Double.parseDouble(input);
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }
    public static boolean tryParseInt(String input){
        try {
            Integer.parseInt(input);
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }
}
