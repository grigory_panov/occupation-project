google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawChartsOnLoad);

var SUB_STR_MARK = "key: ";

function getOptionForChartEntities() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", '/getOptions', true);
    xhr.send(null);
    console.log(xhr);
    var select = document.getElementById('chart_entity_1');
    var select2 = document.getElementById('chart_entity_2');
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            var arr = JSON.parse(xhr.responseText);
            var options = Object.values(arr);
            for (var i = 0; i < options.length; i++){
                var opt = document.createElement('option');
                opt.value = options[i];
                opt.innerHTML = options[i];
                select.appendChild(opt);
                var opt2 = document.createElement('option');
                opt2.value = options[i];
                opt2.innerHTML = options[i];
                select2.appendChild(opt2);
                select.selectedIndex = 0;
                select2.selectedIndex = 0;
            }
            reDrawCharts("chart_entity_1");
            reDrawCharts("chart_entity_2");
        }
    }

}

function drawChartsOnLoad() {
    getOptionForChartEntities();
}

function reDrawCharts(id) {
    var xhr = new XMLHttpRequest();
    var e = document.getElementById(id);
    var selectedEntityString = e.options[e.selectedIndex].value;
    var entityNumber = selectedEntityString.substring(selectedEntityString.indexOf(SUB_STR_MARK) + SUB_STR_MARK.length);
    var deviceType = selectedEntityString.substring(1,2);
    var requestArg = deviceType + entityNumber;

    xhr.open("GET", '/getChartDots?entityKey=' + requestArg, true);
    xhr.send(null);
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            var arr = JSON.parse(xhr.responseText);
            // var t = Object.values(arr);
            var data = new google.visualization.DataTable();
            var myMatrix = matrixArray(arr);
            data.addColumn('number', 'X');
            var chartName = deviceType === "F" ? "FE Director" : "Storage Group";
            data.addColumn('number', chartName);
            data.addRows(myMatrix);
            var titleV = (deviceType === "F") ? "Average bucket number" : "Average time bucket * Request count";
            var options = {
                hAxis: {
                    title: 'Time Period Number'
                },
                vAxis: {
                    title: titleV
                }
            };
            var chartId = "chart_div1";
            if (id === "chart_entity_2"){
                chartId = "chart_div2";
            }
            var chart = new google.visualization.LineChart(document.getElementById(chartId));
            chart.draw(data, options);
        }
    };
}