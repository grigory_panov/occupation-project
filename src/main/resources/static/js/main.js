function matrixArray(array){
    var mArray = [];
    for(var i = 0; i < array.length - 1; i = i + 2){
        mArray[i / 2] = [];
        mArray[i / 2][0] = array[i];
        mArray[i / 2][1] = array[i + 1];
    }
    return mArray;
}

function outputUpdate(vol) {
    document.querySelector('#volume').value = vol;
    document.getElementById("go2r").href = "/analyze?threshold=" + vol;
}