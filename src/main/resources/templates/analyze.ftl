<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>The Project</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/main.css"/>
  </head>
    <body>
        <div class="container">
          <div class="header clearfix">
              <nav>
                  <ul class="nav nav-pills pull-right">
                    <li role="presentation"><a href="/">Back to main page</a></li>
                  </ul>
              </nav>
            <h3 class="text-muted">The Project</h3>
          </div>

          <div class="page-header">
            <h2>The project was made by Grigory Panov</h2>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">TASK 1: Show all existing Storage Groups:</h3>
            </div>

            <div class="panel-body">
                <p>${SGs}</p>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">TASK 2: Show all existing FE Directors:</h3>
            </div>

            <div class="panel-body">
                <p>${FEDs}</p>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">TASK 3: Find FE Directors with problems:</h3>
            </div>

            <div class="panel-body">
                <p>${problemFEDs}</p>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">TASK 4: Find Storage Groups that spoil work of FE Directors:</h3>
            </div>

            <div class="panel-body">
                <p>${infSGs}</p><hr>
            </div>
          </div>


        </div>
    </body>
</html>