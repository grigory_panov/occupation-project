<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Project name</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/main.css"/>
  </head>
    <body>
        <div class="container">
          <div class="header clearfix">
              <nav>
                  <ul class="nav nav-pills pull-right">
                    <li role="presentation"><a href="/">Back to main page</a></li>
                  </ul>
              </nav>
            <h3 class="text-muted">The Project</h3>
          </div>

          <div class="page-header">
            <h1>Compare Charts</h1>
          </div>

            <select id="chart_entity_1" onchange="reDrawCharts(id)"></select>

            <div id="chart_div1"></div>

            <select id="chart_entity_2" onchange="reDrawCharts(id)"></select>
            <div id="chart_div2"></div>
        </div>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="application/javascript" src="/js/main.js"></script>
    <script type="application/javascript" src="/js/compare.js"></script>
</body>
</html>