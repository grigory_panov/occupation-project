<!DOCTYPE html>
<html lang="en"
xmlns:th="http://www.thymeleaf.org"
xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity3"
xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<link rel="stylesheet" href="./css/main.css"/>
<head/>

  <body>

    <div class="container">
      <div class="header clearfix">
        <h3 class="text-muted">The Project</h3>
      </div>

      <div class="jumbotron">
        <h1>It's a threshold. The threshold is the critical <br/>average basket that a request can reach. </h1>

        <div class="row">
	        <div class="col-md-8 col-md-offset-2">
		        <p class="lead">
			        <label>Threshold</label>
			        <input type=range min=0 max=10 value=6 id=rid step=0.1 onchange="outputUpdate(value)">
			        <output id=volume>6</output>
		        </p>
	        </div>
	        <div class="col-md-10 col-md-offset-1">
                <div class="btn-group btn-group-justified" role="group">
			        <a class="btn btn-lg btn-default" href="/analyze" role="button" id="go2r">Go to results</a>
			        <a class="btn btn-lg btn-default" href="/downloadXLSX" role="button">Download Charts</a>
			        <a class="btn btn-lg btn-default" href="/compare" role="button">Compare charts</a>
		        </div>
	        </div>
        </div>
      </div>

    </div> <!-- /container -->

    <script type="application/javascript" src="/js/main.js"></script>

  </body>
</html>
